
################################################################################
## Copyright © 2011-2013 Maxime Jeanson for the Zircon Project.               ##
## This file is part of the Zircon Project.                                   ##
##                                                                            ##
## The Zircon Project is free software: you can redistribute it and/or modify ##
## it under the terms of the GNU General Public License as published by the   ##
## Free Software Foundation, either version 3 of the License, or              ##
## (at your option) any later version.                                        ##
##                                                                            ##
## The Zircon Project is distributed in the hope that it will be useful, but  ##
## WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY ##
## or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License    ##
## for more details.                                                          ##
##                                                                            ##
## You should have received a copy of the GNU General Public License along    ##
## with the Zircon Project. If not, see <http://www.gnu.org/licenses/>.       ##
################################################################################

export PRONAME=		
export PRODIR=		
export PROEXT=		
export PROEXEC=		

export BUILDDIR=	$(abspath ./build)
export SCRIPTSDIR=	$(abspath ./scripts)
export ROOTDIR=		$(abspath ./)

export BUILD=		@(make --quiet -C $@.pro)
export LINK=		@(make --quiet -C $(BUILDDIR) $@)

INSTALL=	@(sh $(SCRIPTSDIR)/install.sh)
BACKUP=		@(sh $(SCRIPTSDIR)/backup.sh)
CLEAN=		@(rm -f $(ODIR)/*.o $(ODIR)/*.pasm $(ODIR)/memmap.*.txt $(ODIR)/error/* $(PRODIR))

-include	$(SCRIPTSDIR)/sol.mk

.PHONY: clean all git $(PRODIRS)

all: $(PRODIRS)
#	$(INSTALL)
#	$(BACKUP)
	
clean:
	$(CLEAN)

git: clean
	@(git add -u *)
	@(git add -A *)
	@(git commit -m "Update")
	@(git push -u origin master)
