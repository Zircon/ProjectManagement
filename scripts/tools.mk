################################################################################
## Copyright © 2011-2013 Maxime Jeanson for the Zircon Project.               ##
## This file is part of the Zircon Project.                                   ##
##                                                                            ##
## The Zircon Project is free software: you can redistribute it and/or modify ##
## it under the terms of the GNU General Public License as published by the   ##
## Free Software Foundation, either version 3 of the License, or              ##
## (at your option) any later version.                                        ##
##                                                                            ##
## The Zircon Project is distributed in the hope that it will be useful, but  ##
## WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY ##
## or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License    ##
## for more details.                                                          ##
##                                                                            ##
## You should have received a copy of the GNU General Public License along    ##
## with the Zircon Project. If not, see <http://www.gnu.org/licenses/>.       ##
################################################################################

################################################################################
## This is the tools listing Makefile.                                        ##
################################################################################

NASM=			nasm
CL=				cl
GCC=			i586-elf-gcc
LD=				i586-elf-ld

NASM_FLAGS=		-Z "$(ODIR)/error/$(notdir $*).txt" -f $(OFORMAT) -I $(INCDIR)/ -o "$(OOBJ)"
CL_FLAGS=		/Gd /Zl /Od /GS- /nologo /c /X /I"$(INCDIR)/" /Fo"$(OOBJ)"
GCC_FLAGS=		-m32 -masm=intel -c -fno-builtin -ffreestanding -nostdlib -mno-red-zone -I $(INCDIR)/ -o "$(OOBJ)"
LD_FLAGS=		-Map memmap.$@.txt --allow-multiple-definition -X -nostdlib -o "$(DEST)/$@" -T ../scripts/$@.ld $(OBJ)



################################################################################
## Building definitions                                                       ##
################################################################################

%.o: %.s
	@(echo "    AS:  ($^) -> ($(OOBJ))")
	$(NASM) $(NASM_FLAGS) $^
	
%.o: %.c
	@(echo "    CC:  ($^) -> ($(OOBJ))")
	$(CL) $(CL_FLAGS) $^

%.o: %.cpp
	@(echo "    CXX: ($^) -> ($(OOBJ))")
	$(CL) $(CL_FLAGS) $^

################################################################################
## Preprocessing definitions                                                  ##
################################################################################

%.s: %.s
	@(echo "    PAS: ($^) -> ($(OOBJ))")
	$(PAS)

%.s: %.c
	@(echo "    CC:  ($^) -> ($(OOBJ))")
	$(CC)
	
%.s: %.cpp
	@(echo "    CXX: ($^) -> ($(OOBJ))")
	$(CXX)
	
