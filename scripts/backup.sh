################################################################################
## Copyright © 2011-2012 Maxime Jeanson for the Zircon Project.               ##
## This file is part of the Zircon Project.                                   ##
##                                                                            ##
## The Zircon Project is free software: you can redistribute it and/or modify ##
## it under the terms of the GNU General Public License as published by the   ##
## Free Software Foundation, either version 3 of the License, or              ##
## (at your option) any later version.                                        ##
##                                                                            ##
## The Zircon Project is distributed in the hope that it will be useful, but  ##
## WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY ##
## or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License    ##
## for more details.                                                          ##
##                                                                            ##
## You should have received a copy of the GNU General Public License along    ##
## with the Zircon Project. If not, see <http://www.gnu.org/licenses/>.       ##
################################################################################

################################################################################
## This file provide an local backup subsystem witch permit to manage code by ##
## by build. To disable this utility, delete the line "$(BACKUP)" in the      ##
## Makefile                                                                   ##
################################################################################

#############
## /bin/sh ##
#############

ARCHIVE_DIR="../Backup/Projects/Zircon/`date +%Y-%m-%d`/`date +%H-%M-%S`/"

mkdir -p "$ARCHIVE_DIR"

tar -cf zircon.tar *
gzip -9 zircon.tar

cp zircon.tar.gz $ARCHIVE_DIR
rm zircon.tar.gz

################
## Backup end ##
################