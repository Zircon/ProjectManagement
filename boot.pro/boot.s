;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; Copyright © 2011-2012 Maxime Jeanson for the Zircon Project.               ;;
;; This file is part of the Zircon Project.                                   ;;
;;                                                                            ;;
;; The Zircon Project is free software: you can redistribute it and/or modify ;;
;; it under the terms of the GNU General Public License as published by the   ;;
;; Free Software Foundation, either version 3 of the License, or              ;;
;; (at your option) any later version.                                        ;;
;;                                                                            ;;
;; The Zircon Project is distributed in the hope that it will be useful, but  ;;
;; WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY ;;
;; or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License    ;;
;; for more details.                                                          ;;
;;                                                                            ;;
;; You should have received a copy of the GNU General Public License along    ;;
;; with the Zircon Project. If not, see <http://www.gnu.org/licenses/>.       ;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

[SECTION .mbhdr]

EXTERN _start

EXTERN DATA.END
EXTERN BSS.END

MB1.START:
	.magic: dd 0x1BADB002
	.flags: dd 0x00000003
	.chksm: dd -(0x1BADB002+0x00000003)
	
	dd MB1.START
	dd 0x00100000
	dd DATA.END
	dd BSS.END
	dd _start
MB1.END:

MB2.START:
	.magic: dd 0xE85250D6
	.arch:  dd 0x00000000
	.length: dd MB2.END-MB2.START
	.chksm: dd -(0xE85250D6+(MB2.END-MB2.START))
	
	dw 2, 0
	dd 24
	dd MB2.START
	dd 0x00100000
	dd DATA.END
	dd BSS.END
	dd _start

	dw 3, 0
	dd 12
	dd _start
	
	dw 6, 0
	dd 12
	dd 0
	
	dd 0
	dd 8
MB2.END: