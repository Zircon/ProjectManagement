;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; Copyright © 2011-2012 Maxime Jeanson for the Zircon Project.               ;;
;; This file is part of the Zircon Project.                                   ;;
;;                                                                            ;;
;; The Zircon Project is free software: you can redistribute it and/or modify ;;
;; it under the terms of the GNU General Public License as published by the   ;;
;; Free Software Foundation, either version 3 of the License, or              ;;
;; (at your option) any later version.                                        ;;
;;                                                                            ;;
;; The Zircon Project is distributed in the hope that it will be useful, but  ;;
;; WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY ;;
;; or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License    ;;
;; for more details.                                                          ;;
;;                                                                            ;;
;; You should have received a copy of the GNU General Public License along    ;;
;; with the Zircon Project. If not, see <http://www.gnu.org/licenses/>.       ;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

GLOBAL _start
GLOBAL _IDT__
GLOBAL _GDT__

EXTERN _main
EXTERN STACK.END

%define MB1 0x2BADB002
%define MB2 0x36d76289
%define NULL 0x00000000

_MB: dd 0x00000000
_MB.type: dd 0x00000000

[SECTION .stack]
K_STACK: resb 0x4000
	.end:
[SECTION .text]

[SECTION .bss]
_IDT__: resb 0x800
_GDT__: resb 0x800
[SECTION .text]

_start:
	cmp eax, MB1
	je .mb1

	cmp eax, MB2
	je .mb2

	.noMb:
		mov dword [_MB.type], NULL
		mov dword [_MB], NULL
		jmp GDTSetup

	.mb1:
		mov dword [_MB.type], MB1
		mov [_MB], ebx
		jmp GDTSetup

	.mb2:
		mov dword [_MB.type], MB2
		mov [_MB], ebx
		jmp GDTSetup

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; GDT Setup
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

GDTSetup:
	xor eax, eax
	xor ebx, ebx
	mov [GDTR.base], dword GDTDescs
	mov ebx, GDTDescs.end
	sub ebx, GDTDescs
	sub ebx, 1
	mov [GDTR.limit], word bx
	lgdt [GDTR]
	
reloadSegment:
	jmp 0x08:reloadDone
		
reloadDone:
	mov ax, 0x10
	mov ds, ax
	mov es, ax
	mov fs, ax
	mov gs, ax
	mov ss, ax
	
	mov esp, K_STACK.end
	mov ebp, esp
	
	call doLoop

doLoop:
	mov eax, [_MB]
	push eax
        call _main
	pop eax
	hlt
	jmp doLoop
		
		
		
[SECTION .data]
GDTR:
	.limit: dw GDTDescs.end-GDTDescs
	.base:  dd GDTDescs
	.end:

GDTDescs:
	.null:
		dd 0x00000000
		dd 0x00000000
	.code:
		dw 0xffff
		dw 0x0000
		db 0x00
		db 0x9a
		db 0xcf
		db 0x00
	.data:
		dw 0xffff
		dw 0x0000
		db 0x00
		db 0x92
		db 0xcf
		db 0x00
	.end:
[SECTION .text]
